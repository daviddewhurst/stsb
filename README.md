# stsb

A (very) basic structural time series library using blocks of variables.


## Who cares?

A semi-local linear trend block:
```
sllt = Noise(
    RandomWalk(
        loc=AR1(
            t1=t1,
        ),
        t1=t1,
    ),
    t1=t1,
)
```

One with a changepoint:
```
sllt1 = RandomWalk(
    loc=AR1(
        t1=t1,
    ),
    t1=t1,
)
sllt2 = RandomWalk(
    loc=AR1(
        t1=t1,
    ),
    t1=t1,
)

changepoint_sllt = Noise(
    changepoint_op(
        sllt1,
        sllt2
    ),
    t1=t1
)
```

Weird additive / seasonal / changepoint thing:
```
s = [Seasonal(t1=t1) for _ in range(4)]
random_seasonal = Noise(
    s[0] + s[1] + changepoint_op(s[2], s[3]),
    t1=t1
)
```

Basic stochastic volatility:
```
stoch_vol = Noise(
    Zero(t1=t1),
    t1=t1,
    scale=GeometricRandomWalk(
        t1=t1
    )
)
```

Have fun!
