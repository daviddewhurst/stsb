import numpy as np
import torch
import pyro
from pyro.infer.enum import config_enumerate
import pyro.distributions as dist 


def get_id(obj):
    id_ = str(type(obj)) + str(type(obj).num)
    type(obj).num += 1
    return id_


def get_timesteps(obj):
    return obj.t1 - obj.t0 + 1


def set_time_endpoints(obj, t0, t1):
    if t0 is not None:
        obj.t0 = t0
    if t1 is not None:
        obj.t1 = t1


class Block:
    
    num = 0
    
    def __init__(self, t1=1):
        self.uid = get_id(self)
        self.t0 = 0
        self.t1 = t1
    
    def model(self, *args, **kwargs):
        return 'None'
    
    def __add__(self, right):
        return AddedBlock(self, right)
    
    
class ChangepointBlock(Block):
    
    num = 0
    
    def __init__(
        self,
        left,
        right,
    ):
        super().__init__()
        self.uid = get_id(self)
        self.left = left
        self.right = right

        self.t0 = self.left.t0
        self.t1 = self.right.t1
        self.size = get_timesteps(self) - 2

        if self.size < 1:
            raise ValueError('Must have t1 - (t0 + 1) > 0')
            
    def __repr__(self,):
        return f'cpt({str(self.left)}, {str(self.right)})'
 
    def model(self, t0=None, t1=None):
        """
        Changepoint model.
        
        Nonassociative function concatenation.
        left(t -> t') \oplus right(t' -> T).
        """
        
        # draw the discrete rv
        #cpt_prob = pyro.sample(
        #    self.uid + 'cpt_prob',
        #    dist.Dirichlet(
        #        torch.ones(self.size) / self.size
        #    )
        #)
        #changepoint = pyro.sample(
        #    self.uid + 'changepoint',
        #    dist.Categorical(
        #        cpt_prob
        #    )
        #)
        set_time_endpoints(self, t0, t1)
        timesteps = get_timesteps(self)
        changepoint = pyro.sample(
            self.uid + 'changepoint',
            dist.Uniform(1, self.size + 1)
        )
        changepoint = torch.floor(changepoint).type(torch.LongTensor)
        left_mask = torch.where(
            torch.linspace(self.t0, self.t1, timesteps) < changepoint,
            torch.ones((timesteps,)),
            torch.zeros((timesteps,))
        ).type(torch.BoolTensor)
        right_mask = left_mask.logical_not()

        this_left_model = pyro.poutine.mask(
            self.left.model,
            mask=left_mask
        )
        this_right_model = pyro.poutine.mask(
            self.right.model,
            mask=right_mask
        )

        left_draw = this_left_model()
        right_draw = this_right_model()

        changepointed =  pyro.deterministic(
            self.uid + 'concatenated',
            torch.cat([
                left_draw[:changepoint],
                right_draw[changepoint:]
            ])
        )

        return changepointed
    
    
class AddedBlock(Block):
    
    num = 0
    
    def __init__(
        self,
        left,
        right,
    ):
        super().__init__()
        self.uid = get_id(self)
        # computation is delayed until .model() is called
        # this ensures that each leaf block has model called
        # only once
        self.left = left
        self.right = right
        
        self.t0 = self.left.t0
        self.t1 = self.right.t1
        
    def __repr__(self,):
        return f'{str(self.left)} + {str(self.right)}'
        
    def model(self, *args, **kwargs):
        return self.left.model() + self.right.model()


class RandomWalk(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        loc=None,
        scale=None,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.loc = loc
        self.scale = scale
        
    def __repr__(self,):
        return f'RandomWalk({str(self.loc), str(self.scale)})'
        
    def model(self, t0=None, t1=None,):
        set_time_endpoints(self, t0, t1)

        if issubclass(type(self.loc), Block):
            loc = self.loc.model(t0=t0, t1=t1)
        else:
            loc = pyro.sample(
                self.uid + 'loc',
                dist.Normal(0., 0.1)
            )
        if issubclass(type(self.scale), Block):
            scale = self.scale.model(t0=t0, t1=t1)
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(0., 1.)
            )
        
        timesteps = get_timesteps(self)
        noise = pyro.sample(
            self.uid + 'noise',
            dist.Normal(loc, scale).expand((timesteps,)).to_event(1)
        )
        return pyro.deterministic(self.uid + 'rw', noise.cumsum(-1))
    
    
class GeometricRandomWalk(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        loc=None,
        scale=None,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.loc = loc
        self.scale = scale
        
    def __repr__(self,):
        return f'GeometricRandomWalk({str(self.loc), str(self.scale)})'
        
    def model(
        self,
    ):
        if issubclass(type(self.loc), Block):
            loc = self.loc.model()
        else:
            loc = pyro.sample(
                self.uid + 'loc',
                dist.Normal(0., 0.01)
            )
        if issubclass(type(self.scale), Block):
            scale = self.scale.model()
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(-2.1, 0.1)
            )
        
        timesteps = get_timesteps(self)
        noise = pyro.sample(
            self.uid + 'noise',
            dist.Normal(loc, scale).expand((timesteps,))
        )
        return pyro.deterministic(self.uid + 'rw', noise.cumsum(-1).exp())
    
    
class GlobalTrend(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        a0=None,
        a1=None,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.a0 = a0
        self.a1 = a1
        
    def __repr__(self,):
        return f'GlobalTrend({str(self.a0), str(self.a1)})'
        
    def model(
        self,
    ):
        if issubclass(type(self.a0), Block):
            a0 = self.a0.model()
        else:
            a0 = pyro.sample(
                self.uid + 'a0',
                dist.Normal(0., 0.1)
            )
        if issubclass(type(self.a1), Block):
            a1 = self.a1.model()
        else:
            a1 = pyro.sample(
                self.uid + 'a1',
                dist.Normal(0., 0.1)
            )
        
        timesteps = get_timesteps(self)
        time = torch.linspace(0, timesteps - 1, timesteps)
        return pyro.deterministic(
            self.uid + 'rw',
            a0 + a1 * time
        )
    
    
class AR1(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        beta=None,
        scale=None,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.beta = beta
        self.scale = scale
        
    def __repr__(self,):
        return f'AR1({str(self.beta), str(self.scale)})'
    
    def model(
        self,
    ):
        if issubclass(type(self.beta), Block):
            beta = self.beta.model()
        else:
            beta = pyro.sample(
                self.uid + 'beta',
                dist.Normal(0.5, 0.1)
            )
        if issubclass(type(self.scale), Block):
            scale = self.scale.model()
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(0., 1.)
            )
        ic = pyro.sample(
            self.uid + 'ic',
            dist.Normal(0., 1.)
        )
        timesteps = get_timesteps(self)
        path = torch.empty((timesteps,))
        path[0] = ic
        this_value = ic
        
        for t in pyro.markov(range(1, timesteps)):
            this_value = pyro.sample(
                f'{self.uid}ar-value-{t}',
                dist.Normal(
                    beta * this_value,
                    scale
                )
            )
            path[t] = this_value
        
        return pyro.deterministic(self.uid + 'ar', path)
    
    
class Seasonal(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        period=None,
        scale=None,
        periodic_fn=torch.cos,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.period = period
        self.scale = scale
        self.periodic_fn = periodic_fn
        
    def __repr__(self,):
        return f'Seasonal({str(self.period), str(self.scale)})'
    
    def model(
        self,
    ):
        if issubclass(type(self.period), Block):
            period = self.period.model()
        else:
            period = pyro.sample(
                self.uid + 'period',
                dist.Gamma(30.0, 1.0)
            )
        if issubclass(type(self.scale), Block):
            scale = self.scale.model()
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(0., 1.)
            )
        ic = pyro.sample(
            self.uid + 'ic',
            dist.Normal(0., 1.)
        )
        timesteps = get_timesteps(self)
        time = torch.linspace(0, timesteps - 1, timesteps)
        feature = self.periodic_fn(
            2 * np.pi * time / period
        )
        
        
        return pyro.deterministic(self.uid + 'seasonal', feature)
    
    
class Zero(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        
    def __repr__(self,):
        return '0'

    def model(self,):
        timesteps = get_timesteps(self)
        return torch.zeros((timesteps,))
    
    
class NonMarkov(Block):
    
    num = 0
    
    def __init__(
        self,
        t1=100,
        scale=None,
        fn=lambda t, s, y, noise: torch.zeros(1),
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.scale = scale
        self.fn = fn
        
    def __repr__(self,):
        return f'NonMarkov({str(self.scale)})'
        
    def model(self,):
        timesteps = get_timesteps(self)
        if issubclass(type(self.scale), Block):
            scale = self.scale.model()
            noise = pyro.sample(
                self.uid + 'noise',
                dist.Normal(0., scale)
            )
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(0., 1.)
            )
            noise = pyro.sample(
                self.uid + 'noise',
                dist.Normal(0., scale).expand((timesteps,))
            )
        time = torch.linspace(0, timesteps - 1, timesteps)
        y = torch.empty((timesteps,))
        y[0] = noise[0]
        
        for t in range(1, timesteps):
            y[t] = self.fn(t, time[:t], y[:t], noise[t])
            
        return pyro.deterministic(
            self.uid + 'nonmarkov',
            y
        )
            
        
class Noise(Block):
    
    num = 0
    
    def __init__(
        self,
        loc,
        t1=100,
        scale=None,
    ):
        super().__init__(t1=t1)
        self.uid = get_id(self)
        self.t1 = t1
        self.loc = loc
        self.scale = scale
        
    def __repr__(self,):
        return f'Noise({str(self.loc)}, {str(self.scale)})'
        
        
    @config_enumerate(default='sequential')  # used for changepoints
    def model(
        self,
        data=None,
    ):
        loc = self.loc.model()
        if issubclass(type(self.scale), Block):
            scale = self.scale.model()
        else:
            scale = pyro.sample(
                self.uid + 'scale',
                dist.LogNormal(0., 1.)
            )
        
        obs = pyro.sample(
            self.uid + 'noise',
            dist.Normal(loc, scale).to_event(1),
            obs=data
        )
        return obs
